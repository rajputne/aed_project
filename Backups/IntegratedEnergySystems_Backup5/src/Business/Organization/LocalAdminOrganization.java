/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.LocalEnterpriseRole.LocalOrganizationRole.LocalEnergyOrganizationRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class LocalAdminOrganization extends Organization {

    public LocalAdminOrganization() {
        super(LocalOrganizationType.Admin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new LocalEnergyOrganizationRole());
        return roles;
    }
    
}

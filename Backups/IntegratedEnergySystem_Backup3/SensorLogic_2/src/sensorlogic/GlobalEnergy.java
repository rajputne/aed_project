/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class GlobalEnergy {
    public int energyInKw;

    public int getEnergyInKw() {
        return energyInKw;
    }

    public void setEnergyInKw(int energyInKw) {
        this.energyInKw = energyInKw;
    }
}

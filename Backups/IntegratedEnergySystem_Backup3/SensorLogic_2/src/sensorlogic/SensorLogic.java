/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.Date;

/**
 *
 * @author Neerajsing Rajput
 */
public class SensorLogic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("----One Consumer two devices battery and sensor logic----------");
        Devices devices = new Devices();
        Device device = devices.addDevice();
        device.setDeviceConsumption(5);
        Device d2 = devices.addDevice();
        d2.setDeviceConsumption(29);

        ConsumerBattery b = new ConsumerBattery();

        Battery[] batteries = new Battery[24];
        for (int i = 0; i < batteries.length; i++) {
            batteries[i] = new Battery();
            // batteries[i].setBatteryVolts(14);
        }
        b.setConsumerBatteryArray(batteries);

        b.setDevices(devices);
        Consumers consumerList = new Consumers();
        Consumer consumer = consumerList.addConsumer();
        consumer.setConsumerId(1);
        consumer.setName("Neeraj");

        LocalEnergy le = new LocalEnergy();
        LocalAreaBatteries[] localAreabatteryCells = new LocalAreaBatteries[15];
        Battery[] localbatteryArray = new Battery[25];

        for (int i = 0; i < localbatteryArray.length; i++) {
            localbatteryArray[i] = new Battery();

        }

        for (int i = 0; i < localAreabatteryCells.length; i++) {
            localAreabatteryCells[i] = new LocalAreaBatteries();
            localAreabatteryCells[i].setLocalbatteryArray(localbatteryArray);

        }
        int sum = 0;
        for (int i = 0; i < localAreabatteryCells.length; i++) {
            for (int j = 0; j < localAreabatteryCells[i].getLocalbatteryArray().length; j++) {
                //System.out.println(localbatteryArray[j].getBatteryVolts());
                sum += localbatteryArray[j].getBatteryVolts();

            }

        }
        System.out.println("Total power Given to LocalBattery:" + sum);

        GlobalEnergy ge = new GlobalEnergy();

        Sensor s = new Sensor();

        s.setBattery(b);

        s.setLocalEnergy(le);

        s.setGlobalEnergy(ge);

        consumer.setSensor(s);

        //Area Sensor for Step 2
        le.setTempereture(40);
        le.setWindVelocity(110);
        le.setLocalAreabatteryCells(localAreabatteryCells);
        AreaSensor.setLocalEnergy(le);

        System.out.println(le.getLocalEnergyPower());

        //System.out.println("Consumer battery befor running any devices" + consumer.getSensor().getBattery().getBatteryVolts());
        String Message = consumer.getSensor().smartSwitch();
        //
        System.out.println(Message);
        System.out.println(b.calculatePowerInConsumerBattery());

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class ConsumerBattery {

    private Devices devices;
    private Battery[] consumerBatteryArray;

    public Battery[] getConsumerBatteryArray() {
        return consumerBatteryArray;
    }

    public void setConsumerBatteryArray(Battery[] consumerBatteryArray) {
        this.consumerBatteryArray = consumerBatteryArray;
    }

    public ConsumerBattery() {
        devices = new Devices();
        consumerBatteryArray = new Battery[24];
        
    }

    public Devices getDevices() {
        return devices;
    }

    public void setDevices(Devices devices) {
        this.devices = devices;
    }

    public int getTotalConsumptionByAllDevices() {
        int sum = 0;
        for (Device d : devices.getDeviceList()) {
            sum += d.getDeviceConsumption();
        }
        return sum;
    }

    public int calculatePowerInConsumerBattery() {
        int power = 0;
        int totalPowerInConsumerBattery = 0;
        for (Battery battery : getConsumerBatteryArray()) {
            totalPowerInConsumerBattery += battery.getBatteryVolts();

            power = totalPowerInConsumerBattery - getTotalConsumptionByAllDevices();

            battery.setBatteryVolts(power);

        }
        return power;
    }

}

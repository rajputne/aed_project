/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class AreaSensor {

    private static LocalEnergy localEnergy;

    public static LocalEnergy getLocalEnergy() {
        return localEnergy;
    }

    public static void setLocalEnergy(LocalEnergy localEnergy) {
        AreaSensor.localEnergy = localEnergy;
    }

    public AreaSensor() {
        localEnergy = new LocalEnergy();
    }

    public static LocalEnergy rechargeLocalBattery() {
        LocalEnergy localEnergy = getLocalEnergy();
        LocalAreaBatteries[] localAreabatteryCells = localEnergy.getLocalAreabatteryCells();
         
        for (int i = 0; i < localEnergy.getLocalAreabatteryCells().length; i++) {
            Battery[] localbatteryArray=localAreabatteryCells[i].getLocalbatteryArray();
            for (int j = 0; j < localAreabatteryCells[i].getLocalbatteryArray().length; j++) {
                Battery localBattery = localbatteryArray[j];
                if (localBattery.getBatteryVolts() < 14 && localBattery.getBatteryVolts() > 0) {
                    if (localEnergy.getTempereture() < 35 && localEnergy.getWindVelocity() < 100) {
                        return localEnergy;

                    } else if (localEnergy.getTempereture() > 35 || localEnergy.getWindVelocity() > 100) {
                        int localEnergyVolts = localBattery.getBatteryVolts();

                        if (localEnergyVolts > 14) {
                            return localEnergy;
                        } else {
                            //If the weather condition are good the battery will be fully charged
                            int rechargeThreshHold = 14 - localEnergyVolts;
                            localBattery.setBatteryVolts(localEnergyVolts + rechargeThreshHold);
                            return localEnergy;
                        }
                    }
                } else {
                    //localBattery.setBatteryVolts(0);
                    return localEnergy;
                }
                return localEnergy;

            }

        }

        return localEnergy;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.Date;

/**
 *
 * @author Neerajsing Rajput
 */
public class SensorLogic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Consumers consumerList = new Consumers();
        Devices devices = new Devices();
        Device device = devices.addDevice();
        device.setDeviceConsumption(90);
        Device d2 = devices.addDevice();
        d2.setDeviceConsumption(30);

        Consumer consumer = consumerList.addConsumer();
        consumer.setConsumerId(1);
        consumer.setName("Neeraj");
        consumer.setDevices(devices);
        int powerConsumption = consumer.getTotalConsumptionByAllDevices();

        LocalEnergy le = new LocalEnergy();
        le.setEnergyInKw(1000);
        le.setTempereture(9);
        le.setWindVelocity(9);
        GlobalEnergy ge = new GlobalEnergy();
        ge.setEnergyInKw(1000);
        Sensors s = new Sensors();
        s.setLocalEnergy(le);
        s.setGlobalEnergy(ge);
        String Message = s.switchEnergy(40, 40, consumerList);
        System.out.println(Message);
        for (Consumer c : consumerList.getConsumers()) {
            System.out.println(c.getName());
            System.out.println("Global Energy recieved" + c.getGlobalConsumption());
            System.out.println("Local Energy recieved" + c.getLocalConsumption());
            if (!c.getGlobalConsumptionList().isEmpty()) {
                for (Date d : c.getGlobalConsumptionList().keySet()) {
                    System.out.println("Date when setting was done" + d.toString());
                }
                for (Integer i : c.getGlobalConsumptionList().values()) {
                    System.out.println("The global energy recieved " + i.toString());
                }
            }
            if (c.getLocalConsumptionList() == null) {
                for (Date d : c.getLocalConsumptionList().keySet()) {
                    System.out.println("Date when setting was done" + d.toString());
                }
                for (Integer i : c.getLocalConsumptionList().values()) {
                    System.out.println("The Local energy recieved " + i.toString());
                }
            }
            System.out.println("The Global energy Left " + GlobalEnergyAdmin.getGlobalEnergy().getEnergyInKw());
        }
        System.out.println("Powerconsumption by " + consumer.getName() + "is:" + powerConsumption);

        System.out.println("---------------");
        Message = s.switchEnergy(3, 3, consumerList);
        System.out.println(Message);
        for (Consumer c : consumerList.getConsumers()) {
            System.out.println(c.getName());
            System.out.println("Global Consumption" + c.getGlobalConsumption());
            System.out.println("Local Consumption" + c.getLocalConsumption());
            for (Date d : c.getGlobalConsumptionList().keySet()) {
                System.out.println("Date when setting was done" + d.toString());
            }
            for (Integer i : c.getGlobalConsumptionList().values()) {
                System.out.println("The global energy consumed " + i.toString());
            }
            for (Date d : c.getLocalConsumptionList().keySet()) {
                System.out.println("Date when setting was done" + d.toString());
            }
            for (Integer i : c.getLocalConsumptionList().values()) {
                System.out.println("The Local energy consumed " + i.toString());
            }
            System.out.println("The Global energy Left" + GlobalEnergyAdmin.getGlobalEnergy().getEnergyInKw());
        }
        System.out.println("---------------");
        System.out.println("Power remaining by " + consumer.getName() + "is:" + consumer.calculatePowerInLocalBattery());
        Message = s.switchEnergy(10, 10, consumerList);
        System.out.println(Message);
        for (Consumer c : consumerList.getConsumers()) {
            System.out.println(c.getName());
            System.out.println("Global Consumption" + c.getGlobalConsumption());
            System.out.println("Local Consumption" + c.getLocalConsumption());
            for (Date d : c.getGlobalConsumptionList().keySet()) {
                System.out.println("Date when setting was done" + d.toString());
            }
            for (Integer i : c.getGlobalConsumptionList().values()) {
                System.out.println("The global energy consumed " + i.toString());
            }
            for (Date d : c.getLocalConsumptionList().keySet()) {
                System.out.println("Date when setting was done" + d.toString());
            }
            for (Integer i : c.getLocalConsumptionList().values()) {
                System.out.println("The Local energy consumed " + i.toString());
            }
            System.out.println("The Global energy Left" + GlobalEnergyAdmin.getGlobalEnergy().getEnergyInKw());
        }
        System.out.println("---------------");
        Message = s.switchEnergy(11, 11, consumerList);
        System.out.println(Message);
        for (Consumer c : consumerList.getConsumers()) {
            System.out.println(c.getName());
            System.out.println("Global Consumption" + c.getGlobalConsumption());
            System.out.println("Local Consumption" + c.getLocalConsumption());
            for (Date d : c.getGlobalConsumptionList().keySet()) {
                System.out.println("Date when setting was done" + d.toString());
            }
            for (Integer i : c.getGlobalConsumptionList().values()) {
                System.out.println("The global energy consumed " + i.toString());
            }
            for (Date d : c.getLocalConsumptionList().keySet()) {
                System.out.println("Date when setting was done" + d.toString());
            }
            for (Integer i : c.getLocalConsumptionList().values()) {
                System.out.println("The Local energy consumed " + i.toString());
            }
            System.out.println("The Global energy Left" + GlobalEnergyAdmin.getGlobalEnergy().getEnergyInKw());
        }
        System.out.println(consumer.getBatteryPower());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Neerajsing Rajput
 */
public class Consumer {

    private String name;
    private int consumerId;
    private int globalConsumption;
    private int localConsumption;
    private Map<Date, Integer> globalConsumptionList;
    private Map<Date, Integer> localConsumptionList;
    private Devices devices;
    private int batteryPower;

    public int getBatteryPower() {
        return getGlobalConsumption()+getLocalConsumption();
    }
    
    

    public Devices getDevices() {
        return devices;
    }

    public void setDevices(Devices devices) {
        this.devices = devices;
    }

    public Consumer() {
        globalConsumptionList = new HashMap<>();
        localConsumptionList = new HashMap<>();
        devices = new Devices();
    }

    public Map<Date, Integer> getLocalConsumptionList() {
        return localConsumptionList;
    }

    public void setLocalConsumptionList(Map<Date, Integer> localConsumptionList) {
        this.localConsumptionList = localConsumptionList;
    }

    public Map<Date, Integer> getGlobalConsumptionList() {
        return globalConsumptionList;
    }

    public void setGlobalConsumptionList(Map<Date, Integer> globalConsumptionList) {
        this.globalConsumptionList = globalConsumptionList;
    }

    public int getGlobalConsumption() {
        return globalConsumption;
    }

    public void setGlobalConsumption(int globalConsumption) {
        this.globalConsumption = globalConsumption;
    }

    public int getLocalConsumption() {
        return localConsumption;
    }

    public void setLocalConsumption(int localConsumption) {
        this.localConsumption = localConsumption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(int consumerId) {
        this.consumerId = consumerId;
    }

    public int getTotalConsumptionByAllDevices() {
        int sum = 0;
        for (Device d : devices.getDeviceList()) {
            sum += d.getDeviceConsumption();
        }
        return sum;
    }

    public int calculatePowerInLocalBattery() {
        int power = 0;
        power = getLocalConsumption() - getTotalConsumptionByAllDevices();
        return power;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class Consumers {

    ArrayList<Consumer> consumerList;

    public ArrayList<Consumer> getConsumerList() {
        return consumerList;
    }

    public void setConsumerList(ArrayList<Consumer> consumerList) {
        this.consumerList = consumerList;
    }

    
    public Consumers() {
        consumerList=new ArrayList<>();
    }
    public Consumer addConsumer() {
        Consumer consumer = new Consumer();
        consumerList.add(consumer);
        return consumer;
    }
}

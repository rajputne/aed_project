/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Neerajsing Rajput
 */
public class Sensor {

    private GlobalEnergy globalEnergy;
    private LocalEnergy localEnergy;
    private ConsumerBattery battery;
    private Integer consumptionCalculation;

    public ConsumerBattery getBattery() {
        return battery;
    }

    public void setBattery(ConsumerBattery battery) {
        this.battery = battery;
    }

    //Since Prof Bugrara mentioned that datapoints have to be captured My Datapoints
    private Map<Date, Integer> globalConsumptionList;
    private Map<Date, Integer> localConsumptionList;
    public Map<Date, Integer> tempValues;

    public Map<Date, Integer> getGlobalConsumptionList() {
        return globalConsumptionList;
    }

    public void setGlobalConsumptionList(Map<Date, Integer> globalConsumptionList) {
        this.globalConsumptionList = globalConsumptionList;
    }

    public Map<Date, Integer> getLocalConsumptionList() {
        return localConsumptionList;
    }

    public void setLocalConsumptionList(Map<Date, Integer> localConsumptionList) {
        this.localConsumptionList = localConsumptionList;
    }

    public Sensor() {
        battery = new ConsumerBattery();
        localEnergy = new LocalEnergy();
        globalEnergy = new GlobalEnergy();
        globalConsumptionList = new HashMap<>();
        localConsumptionList = new HashMap<>();
        tempValues = new HashMap<Date, Integer>();
        consumptionCalculation = 0;
    }

    public GlobalEnergy getGlobalEnergy() {
        return globalEnergy;
    }

    public void setGlobalEnergy(GlobalEnergy globalEnergy) {
        this.globalEnergy = globalEnergy;
    }

    public LocalEnergy getLocalEnergy() {
        return localEnergy;
    }

    public void setLocalEnergy(LocalEnergy localEnergy) {
        this.localEnergy = localEnergy;
    }

    public String smartSwitch() {
        //First check the charge in the LocalArea and then perform the operation
        setLocalEnergy(AreaSensor.rechargeLocalBattery());

        LocalEnergy le = getLocalEnergy();
        GlobalEnergy ge = getGlobalEnergy();
        ConsumerBattery b = getBattery();

        //To See weather the conumption is done or not
        b.getTotalConsumptionByAllDevices();
        b.calculateVoltageInConsumerBattery();
        BatteryComparatorByByID orderById = new BatteryComparatorByByID();
        Collections.sort(le.getLocalAreabattery().getLocalbatteryList(), orderById);
        if (b.getBatteryVolts() <= 306 && b.getBatteryVolts() > 0) {
            //Take From the Local Energy Bank First
            for (Battery localAreaBattery : le.getLocalAreabattery().getLocalbatteryList()) {
                //Check the all local Area battery should be above 306 voltage
                //Find the Maximum value in this localAreaBattery arrayList and store that 
                //in a variable then apply the belwo check
                
                Battery maxBattery = le.getLocalAreabattery().getLocalbatteryList().get(le.getLocalAreabattery().getLocalbatteryList().size() - 1);
                if (maxBattery.getBatteryVolts() > 306) {

                    if (localAreaBattery.getBatteryVolts() > 0) {
                        //Check whether the local Battery is the maxBattery Battery
                        if (maxBattery.getBatteryVolts() == localAreaBattery.getBatteryVolts()) {
                            //Recharge consumer battery
                            b.setBatteryVolts(b.getTotalConsumptionByAllDevices() + b.getBatteryVolts());
                            //Discharge Local Battery
                            localAreaBattery.setBatteryVolts(maxBattery.getBatteryVolts() - b.getTotalConsumptionByAllDevices());
                            Date date = new Date();
                            tempValues.put(date, b.getTotalConsumptionByAllDevices());
                            setLocalConsumptionList(tempValues);
                            return "The Local Energy is switch On and the consmption made by you" + b.getTotalConsumptionByAllDevices();
                        }

                    } else {
                        return "1 Battery Out";
                    }
                } else {
                    //Write the consumption code here.
                    int globalConsumption=b.getPowerInKwh();
                    
                    return "The Global Energy is switch On and the consmption made by you" + consumptionCalculation;

                }

            }

        }
        return "The consumption made by you is above the battery Load its faulty. Trip Fuse";
    }

}

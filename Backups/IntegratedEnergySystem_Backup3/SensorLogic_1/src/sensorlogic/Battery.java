/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.Collections;

/**
 *
 * @author Neerajsing Rajput
 */
public class Battery {

    private int batteryVolts;

    public Battery() {
        
    }

    public int getBatteryVolts() {
        return batteryVolts;
    }

    public void setBatteryVolts(int batteryVolts) {
        this.batteryVolts = batteryVolts;
    }

    public int compareTo(Battery o) {
        if (this.batteryVolts > o.getBatteryVolts()) {
            return -1;
        } else if (this.batteryVolts < o.getBatteryVolts()) {
            return 1;
        } else {

            return 0;
        }

    }
}

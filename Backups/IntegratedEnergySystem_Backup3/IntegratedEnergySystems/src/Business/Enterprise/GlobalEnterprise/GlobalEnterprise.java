/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.GlobalEnterprise;

import Business.Enterprise.Enterprise;
import Business.Role.GlobalEnterprise.GlobaEnterpriselAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class GlobalEnterprise extends Enterprise {

    public GlobalEnterprise(String name, EnterpriseType type) {
        super(name, type.GlobalEnergy);
        
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new GlobaEnterpriselAdminRole());
        return roles; //To change body of generated methods, choose Tools | Templates.
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GlobalEnergy;

import java.util.Calendar;

/**
 *
 * @author neera
 */
public class EnergyPerYear {

    EnergyPerDay epd;
    int year;

    public int calculateEnergyConsumptionOfCurrentYear(boolean isLeap) {
        int sum = 0;
        if (isLeap) {
            for (int i = 0; i < 365; i++) {
                sum += epd.sum_Of_Consumption_Per_Day();

            }
        } else {
            for (int i = 0; i < 366; i++) {
                sum += epd.sum_Of_Consumption_Per_Day();
            }

        }
        return sum;

    }
//Get the result of this and put in the aboce menthod

    public static boolean isLeapYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(cal.DAY_OF_YEAR) > 365;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.GlobalManagerToLocalManager;

import Business.Employee.Consumer.Consumer;
import Business.WorkQueue.WorkRequest;

/**
 *
 * @author raunak
 */
public class SetConsumerWorkRequest extends WorkRequest {

    private Consumer consumer;

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public SetConsumerWorkRequest() {
        consumer = new Consumer();
    }
    
    

    @Override
    public String toString() {
        return consumer.getName();
    }

}

package Business;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    //
    public boolean checkIfUsernameIsUnique(String username) {

        Boolean isUserNameUnique = false;
        //Do some coding to check un
        if (this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            isUserNameUnique = false;
        }
        for (Network network : networkList) {
            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                if (!ent.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
                    for (Organization org : ent.getOrganizationDirectory().getOrganizationList()) {
                        if (!org.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
                            isUserNameUnique = false;
                        } else {
                            isUserNameUnique = true;
                            break;
                        }
                    }
                    isUserNameUnique = false;
                } else {
                    isUserNameUnique = true;
                    break;
                }
            }
        }
        return isUserNameUnique;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.LocalManagerRole;

import Business.Employee.Consumer.Battery;
import Business.LocalEnergy.CommunitySensor;
import Business.Employee.Consumer.ConsumerBattery;
import Business.LocalEnergy.LocalEnergy;
import Business.EcoSystem;
import Business.Employee.Consumer.Consumer;
import Business.Employee.Consumer.Sensor;
import Business.Enterprise.Enterprise;
import static Business.Enterprise.Enterprise.EnterpriseType.LocalEnergy;
import Business.Network.Network;
import Business.Organization.ConsumerOrganization;
import Business.Organization.LocalEnergy.LocalAreaOrganization;
import Business.Organization.LocalEnergy.LocalManagerEnergyOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.ConsumerRequests.RequestsFromConsumer;
import Business.WorkQueue.GlobalManagerToLocalManager.SetConsumerWorkRequest;
import Business.WorkQueue.LocalOrganizationToLocalManager.LocalOrganizationToLocalManagerSetLocalEnergyWR;
import Business.WorkQueue.ConsumerRequests.CommunitySensorForConsumer;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author raunak
 */
public class LocalManagerWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount userAccount;
    private LocalManagerEnergyOrganization LocalOrganization;
    LocalOrganizationToLocalManagerSetLocalEnergyWR setLocalEnergyToCommSensor;
    private UserAccount userAccountOfConsumer;
    private CommunitySensor communitySensor;
    private SetConsumerWorkRequest request;

    /**
     * Creates new form LabAssistantWorkAreaJPanel
     */
    public LocalManagerWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, EcoSystem business) {
        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.business = business;
        this.LocalOrganization = (LocalManagerEnergyOrganization) organization;
        userAccountOfConsumer = new UserAccount();
        request = new SetConsumerWorkRequest();
        setLocalEnergyToCommSensor = new LocalOrganizationToLocalManagerSetLocalEnergyWR();
        communitySensor = new CommunitySensor();
        populateConsumerTable();
        populateWorkRequestForLocalEnergy();
        populateWorkRequestComingFromConsumer();
    }

    public void populateWorkRequestComingFromConsumer() {
        DefaultTableModel model = (DefaultTableModel) consumerRequest.getModel();

        model.setRowCount(0);

        for (WorkRequest request : LocalOrganization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof RequestsFromConsumer) {
                Object[] row = new Object[4];
                row[0] = request;
                row[1] = request.getMessage();
                row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[3] = request.getStatus();

                model.addRow(row);
            }

        }

    }

    public void populateWorkRequestForLocalEnergy() {
        DefaultTableModel model = (DefaultTableModel) LocalOrganiztionRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : LocalOrganization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof LocalOrganizationToLocalManagerSetLocalEnergyWR) {
                Object[] row = new Object[6];
                row[0] = request;
                row[1] = ((LocalOrganizationToLocalManagerSetLocalEnergyWR) request).getLocalEnergy().getLocalAreabattery().getLocalbatteryList().size();
                row[2] = request.getMessage();
                row[3] = request.getSender();
                row[4] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[5] = request.getStatus();

                model.addRow(row);
            }
        }
    }

    public void populateConsumerTable() {
        DefaultTableModel model = (DefaultTableModel) consumerListWorkRequestFromGlobal.getModel();

        model.setRowCount(0);

        for (WorkRequest request : LocalOrganization.getWorkQueue().getWorkRequestList()) {
            if (request instanceof SetConsumerWorkRequest) {
                Object[] row = new Object[9];
                row[0] = request;
                row[1] = ((SetConsumerWorkRequest) request).getConsumer().getAddress();
                row[2] = request.getMessage();
                row[3] = request.getSender();
                row[4] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[5] = request.getStatus();
                row[6] = ((SetConsumerWorkRequest) request).getConsumer().getLocalAreaCode();
                row[7] = ((SetConsumerWorkRequest) request).getConsumer().getConsumerId();
                row[8] = ((SetConsumerWorkRequest) request).getConsumer().getSensor().getUniqueSensorId();
                model.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        LocalOrganiztionRequestJTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        refreshJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnGetLocalEnergy = new javax.swing.JButton();
        txtLocalEnergy = new javax.swing.JTextField();
        btnAssignConsumerToCommunitySensor = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        consumerRequest = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        btnAssign2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        consumerListWorkRequestFromGlobal = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        btnAssign3 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        btnFindUserAccount = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LocalOrganiztionRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Local Energy", "No. Battery", "Message", "Sender", "Reciever", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(LocalOrganiztionRequestJTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 550, 96));

        assignJButton.setText("Assign Battery to community");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });
        add(assignJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 130, 260, 40));

        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });
        add(processJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 620, -1, -1));

        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });
        add(refreshJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 80, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Step3. Select the Consumer whom you want to assign");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, -1, -1));

        jLabel2.setText("Local Energy");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, -1, -1));

        btnGetLocalEnergy.setText("Get Local Energy");
        btnGetLocalEnergy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGetLocalEnergyActionPerformed(evt);
            }
        });
        add(btnGetLocalEnergy, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 620, -1, -1));
        add(txtLocalEnergy, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 590, 140, -1));

        btnAssignConsumerToCommunitySensor.setText("Assign sensor to user");
        btnAssignConsumerToCommunitySensor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssignConsumerToCommunitySensorActionPerformed(evt);
            }
        });
        add(btnAssignConsumerToCommunitySensor, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 430, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Step 1. Choose Local Area Battery for a consumer");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, -1, -1));

        consumerRequest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Address", "Message", "Sender", "Reciever", "Status", "Local Code", "Consumer Id", "Sensor Id"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(consumerRequest);

        add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 460, 710, 96));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Step 2. Select the consumer add some parameters");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 420, -1, -1));

        btnAssign2.setText("Inform User");
        btnAssign2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssign2ActionPerformed(evt);
            }
        });
        add(btnAssign2, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 570, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setText("Local Manager Work Area Panel");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 30, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("Community Sensor");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 250, -1, -1));

        consumerListWorkRequestFromGlobal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Address", "Message", "Sender", "Reciever", "Status", "Local Code", "Consumer Id", "Sensor Id"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(consumerListWorkRequestFromGlobal);

        add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, 710, 96));

        jButton1.setText("Process Consumer");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 270, -1, -1));

        btnAssign3.setText("Get User Details From Table 2");
        btnAssign3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssign3ActionPerformed(evt);
            }
        });
        add(btnAssign3, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 310, -1, -1));

        jLabel7.setText("OR");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 340, -1, -1));

        btnFindUserAccount.setText("Find the selected user Account");
        btnFindUserAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindUserAccountActionPerformed(evt);
            }
        });
        add(btnFindUserAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 370, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = LocalOrganiztionRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        LocalOrganizationToLocalManagerSetLocalEnergyWR setLocalEnergyToCommSensor = (LocalOrganizationToLocalManagerSetLocalEnergyWR) LocalOrganiztionRequestJTable.getValueAt(selectedRow, 0);
        int numberOfBatteries = setLocalEnergyToCommSensor.getLocalEnergy().getLocalAreabattery().getLocalbatteryList().size();
        ArrayList<Battery> tempBatteryList = new ArrayList<>();
        for (Battery battery : setLocalEnergyToCommSensor.getLocalEnergy().getLocalAreabattery().getLocalbatteryList()) {
            //battery = setLocalEnergyToCommSensor.getLocalEnergy().getLocalAreabattery().addLocalBattery();
            battery.setBatteryVolts(CommunitySensor.MAX_VOLTAGE);
            tempBatteryList.add(battery);
        }
        setLocalEnergyToCommSensor.getLocalEnergy().getLocalAreabattery().setLocalbatteryList(tempBatteryList);
        communitySensor.setLocalEnergy(setLocalEnergyToCommSensor.getLocalEnergy());
        setLocalEnergyToCommSensor.setStatus("Complete ");
        setLocalEnergyToCommSensor.setReceiver(userAccount);
        setLocalEnergyToCommSensor.setMessage("I am using this battery");
        setLocalEnergyToCommSensor.setStatus("In use");
        //Send it back to the Local Organization
        JOptionPane.showMessageDialog(null, "The Community sensor is set and number of batteries" + numberOfBatteries);
        populateConsumerTable();
        populateWorkRequestForLocalEnergy();

    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = LocalOrganiztionRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        LocalOrganizationToLocalManagerSetLocalEnergyWR setLocalEnergyToCommSensor = (LocalOrganizationToLocalManagerSetLocalEnergyWR) LocalOrganiztionRequestJTable.getValueAt(selectedRow, 0);

        int numberOfBatteries = setLocalEnergyToCommSensor.getLocalEnergy().getLocalAreabattery().getLocalbatteryList().size();
        for (int i = 0; i < numberOfBatteries; i++) {
            Battery battery = setLocalEnergyToCommSensor.getLocalEnergy().getLocalAreabattery().addLocalBattery();
            battery.setBatteryVolts(CommunitySensor.MAX_VOLTAGE);
            System.out.println(i);
        }
        communitySensor.setLocalEnergy(setLocalEnergyToCommSensor.getLocalEnergy());

        //
        //  ProcessWorkRequestJPanel processWorkRequestJPanel = new ProcessWorkRequestJPanel(userProcessContainer, userAccount, setLocalEnergyToCommSensor, (LocalManagerEnergyOrganization) LocalOrganization, business);
        //userProcessContainer.add("processWorkRequestJPanel", processWorkRequestJPanel);
        //CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        //layout.next(userProcessContainer);

    }//GEN-LAST:event_processJButtonActionPerformed

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateConsumerTable();
        populateWorkRequestForLocalEnergy();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void btnGetLocalEnergyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGetLocalEnergyActionPerformed
        // TODO add your handling code here:
        int sizeOfLocalBattery = 0;
        int tempereture = 0;
        int windVeloity = 0;
        for (WorkRequest wr : LocalOrganization.getWorkQueue().getWorkRequestList()) {
            if (wr instanceof LocalOrganizationToLocalManagerSetLocalEnergyWR) {
                setLocalEnergyToCommSensor = (LocalOrganizationToLocalManagerSetLocalEnergyWR) wr;
                sizeOfLocalBattery = ((LocalOrganizationToLocalManagerSetLocalEnergyWR) wr).getLocalEnergy().getLocalAreabattery().getLocalbatteryList().size();
                tempereture = ((LocalOrganizationToLocalManagerSetLocalEnergyWR) wr).getLocalEnergy().getTempereture();
                windVeloity = ((LocalOrganizationToLocalManagerSetLocalEnergyWR) wr).getLocalEnergy().getWindVelocity();
            }
        }
        String a = String.valueOf(sizeOfLocalBattery);
        txtLocalEnergy.setText(a);
    }//GEN-LAST:event_btnGetLocalEnergyActionPerformed

    private void btnAssignConsumerToCommunitySensorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssignConsumerToCommunitySensorActionPerformed
        // TODO add your handling code here:
        int selectedRow = consumerListWorkRequestFromGlobal.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        request = (SetConsumerWorkRequest) consumerListWorkRequestFromGlobal.getValueAt(selectedRow, 0);
        request.getConsumer().setSensor(new Sensor());
        //Assign him battery
        ConsumerBattery b = new ConsumerBattery();
        b.setBatteryVolts(CommunitySensor.MAX_VOLTAGE);
        request.getConsumer().getSensor().setBattery(b);
        //Setting Battery

        //To Assign Work Request
        CommunitySensorForConsumer setCommunitySensorForConsumer = new CommunitySensorForConsumer();
        setCommunitySensorForConsumer.setCommunitySensor(communitySensor);

        for (Network n : business.getNetworkList()) {
            for (Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()) {
                if (e.getEnterpriseType() == Enterprise.EnterpriseType.LocalEnergy) {
                    e.getWorkQueue().getWorkRequestList().add(request);
                }
                for (Organization organiz : e.getOrganizationDirectory().getOrganizationList()) {
                    if (organiz instanceof ConsumerOrganization) {
                        //Sending to all the consumers Not required
                        // org.getWorkQueue().getWorkRequestList().add(setLocalEnergyToCommSensor);

                        for (UserAccount u : organiz.getUserAccountDirectory().getUserAccountList()) {
                            if (u.equals(userAccountOfConsumer)) {

                                u.getWorkQueue().getWorkRequestList().add(setCommunitySensorForConsumer);
                                JOptionPane.showMessageDialog(null, "Alloting consumer local Energy:" + u.getEmployee());
                            }
                        }
                    }
                }
            }
        }

    }//GEN-LAST:event_btnAssignConsumerToCommunitySensorActionPerformed

    private void btnAssign2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssign2ActionPerformed
        // TODO add your handling code here:
        int selectedRow = consumerRequest.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }
        RequestsFromConsumer request = (RequestsFromConsumer) consumerRequest.getValueAt(selectedRow, 0);
        userAccountOfConsumer = request.getSender();

        request.setConsumer(request.getConsumer());
        // LocalManagerSettingConsumerParameters localManagerSettingConsumerParameters = new LocalManagerSettingConsumerParameters(userProcessContainer, userAccount, request, consumer, (LocalManagerEnergyOrganization) LocalOrganization, business);
        // userProcessContainer.add("localManagerSettingConsumerParameters", localManagerSettingConsumerParameters);
        // CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        // layout.next(userProcessContainer);

        JOptionPane.showMessageDialog(null, "The user selected is " + userAccountOfConsumer.getEmployee().getName());

    }//GEN-LAST:event_btnAssign2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        int selectedRow = consumerListWorkRequestFromGlobal.getSelectedRow();
        if (selectedRow < 0) {
            return;
        }
        request = (SetConsumerWorkRequest) consumerListWorkRequestFromGlobal.getValueAt(selectedRow, 0);
        LocalManagerSettingConsumerParameters localManagerSettingConsumerParameters = new LocalManagerSettingConsumerParameters(userProcessContainer, userAccount, request, request.getConsumer(), (LocalManagerEnergyOrganization) LocalOrganization, business);
        userProcessContainer.add("localManagerSettingConsumerParameters", localManagerSettingConsumerParameters);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnAssign3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssign3ActionPerformed
        // TODO add your handling code here:

        int selectedRow = consumerRequest.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }
        RequestsFromConsumer request = (RequestsFromConsumer) consumerRequest.getValueAt(selectedRow, 0);
        userAccountOfConsumer = request.getSender();
        
        JOptionPane.showMessageDialog(null, "The user selected is " + userAccountOfConsumer.getEmployee().getName());
    }//GEN-LAST:event_btnAssign3ActionPerformed

    private void btnFindUserAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindUserAccountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnFindUserAccountActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable LocalOrganiztionRequestJTable;
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton btnAssign2;
    private javax.swing.JButton btnAssign3;
    private javax.swing.JButton btnAssignConsumerToCommunitySensor;
    private javax.swing.JButton btnFindUserAccount;
    private javax.swing.JButton btnGetLocalEnergy;
    private javax.swing.JTable consumerListWorkRequestFromGlobal;
    private javax.swing.JTable consumerRequest;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JButton processJButton;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JTextField txtLocalEnergy;
    // End of variables declaration//GEN-END:variables
}

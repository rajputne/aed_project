/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.Date;

/**
 *
 * @author Neerajsing Rajput
 */
public class SensorLogic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("----Two Consumer two devices battery and sensor logic----------");
        Devices devices = new Devices();
        Device device = devices.addDevice();
        device.setPower(6);
        device.setPower(15);
        
        device.setDeviceConsumptionEquivalentInVoltage(30);
        Device d2 = devices.addDevice();
        d2.setDeviceConsumptionEquivalentInVoltage(16);

        ConsumerBattery b = new ConsumerBattery();
        b.setBatteryVolts(336);
        b.setDevices(devices);
        Consumers consumerList = new Consumers();
        Consumer consumer = consumerList.addConsumer();
        consumer.setConsumerId(1);
        consumer.setName("Neeraj");

        Consumer consumer2 = consumerList.addConsumer();
        consumer2.setConsumerId(2);
        consumer.setName("Komalsingh");

        LocalEnergy le = new LocalEnergy();
        //Setting 10 Battreies at 140 volts each

        for (int i = 1; i < 10; i++) {
            Battery battery = le.getLocalAreabattery().addLocalBattery();
            battery.setBatteryVolts(336);

        }
        System.out.println("The Energy in Local Bank" + le.getLocalAreabattery().getLocalEnergyVoltage());

        GlobalEnergy ge = new GlobalEnergy();

        Sensor s = new Sensor();
        s.setBattery(b);
        s.setLocalEnergy(le);
        s.setGlobalEnergy(ge);
        consumer.setSensor(s);
        consumer2.setSensor(s);
        //Area Sensor for Step 2
        le.setTempereture(40);
        le.setWindVelocity(110);
        AreaSensor.setLocalEnergy(le);

        System.out.println("Consumer after befor running any devices" + consumer.getSensor().getBattery().getBatteryVolts());

        System.out.println("Consumer2 battery befor running any devices" + consumer2.getSensor().getBattery().getBatteryVolts());
        String Message = consumer.getSensor().smartSwitch();
        System.out.println(Message);
        System.out.println("Consumer battery after running any devices" + consumer.getSensor().getBattery().getBatteryVolts());
        for (Battery bat : le.getLocalAreabattery().getLocalbatteryList()) {
            System.out.println("The Local energy in the batteries " + bat.getBatteryVolts());

        }
        le.setTempereture(5);
        le.setWindVelocity(5);
        String Message2 = consumer2.getSensor().smartSwitch();
        System.out.println(Message2);
        System.out.println("Consumer22 battery after running any devices" + consumer2.getSensor().getBattery().getBatteryVolts());
        
        for (Battery bat : le.getLocalAreabattery().getLocalbatteryList()) {
            System.out.println("The Local energy in the batteries " + bat.getBatteryVolts());

        }
        consumer.getSensor().tempValues.keySet().stream().forEach((d) -> {
            System.out.println("Date when setting was done" + d.toString());
        });
        for (Integer i : consumer.getSensor().getGlobalConsumptionList().values()) {
            System.out.println("The global energy consumed " + i.toString());
            
        }
        for (Date d : consumer.getSensor().getLocalConsumptionList().keySet()) {
            System.out.println("Date when setting was done" + d.toString());

        }
        for (Integer i : consumer.getSensor().getLocalConsumptionList().values()) {
            System.out.println("The Local energy consumed " + i.toString());
        }
        //Check Which battery is being used
        
        System.out.println("---------------------");
        le.setTempereture(40);
        le.setWindVelocity(110);
        
        AreaSensor.rechargeLocalBattery();
        for (Battery bat : le.getLocalAreabattery().getLocalbatteryList()) {
            System.out.println("The Local energy in the batteries " + bat.getBatteryVolts());

        }
        System.out.println("---------");
        AreaSensor.rechargeLocalBattery();
        for (Battery bat : le.getLocalAreabattery().getLocalbatteryList()) {
            System.out.println("The Local energy in the batteries " + bat.getBatteryVolts());

        }
        System.out.println("The Consumer energy in the batteries " + consumer.getSensor().getBattery().getBatteryVolts());

        System.out.println(consumer.getSensor().getBattery().calculateVoltageInConsumerBattery());
    }
}

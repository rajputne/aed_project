/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class Area {

    ArrayList<Consumers> area;

    public ArrayList<Consumers> getArea() {
        return area;
    }

    public void setArea(ArrayList<Consumers> area) {
        this.area = area;
    }

    public Area() {
        area = new ArrayList<>();

    }

    public Consumers addConsumers() {
        Consumers consumers = new Consumers();
        area.add(consumers);
        return consumers;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class AreaSensor {

    private static LocalEnergy localEnergy;

    public static LocalEnergy getLocalEnergy() {
        return localEnergy;
    }

    public static void setLocalEnergy(LocalEnergy localEnergy) {
        AreaSensor.localEnergy = localEnergy;
    }

    public AreaSensor() {
        localEnergy = new LocalEnergy();
    }

    //Check all the batteries in array should be recharged 
    public static LocalEnergy rechargeLocalBattery() {
        LocalEnergy localEnergy = getLocalEnergy();
        for (Battery localBattery : localEnergy.getLocalAreabattery().getLocalbatteryList()) {
            if (localBattery.getBatteryVolts() < 306 && localBattery.getBatteryVolts() > 0) {
                if (localEnergy.getTempereture() < 35 && localEnergy.getWindVelocity() < 100) {
                    return localEnergy;

                } else if (localEnergy.getTempereture() > 35 || localEnergy.getWindVelocity() > 100) {
                    int localEnergyVolts = localBattery.getBatteryVolts();

                    if (localEnergyVolts > 336) {
                        return localEnergy;
                    } else {
                        //If the weather condition are good the battery will be fully charged

                        if (localBattery.getBatteryVolts() < 336) {
                            int rechargeThreshHold = 336 - localEnergyVolts;
                            localBattery.setBatteryVolts(localEnergyVolts + rechargeThreshHold);
                        } else {
                            return localEnergy;
                        }
                    }
                }
            } else {
                //localBattery.setBatteryVolts(0);
                return localEnergy;
            }

        }
        return localEnergy;
    }
}

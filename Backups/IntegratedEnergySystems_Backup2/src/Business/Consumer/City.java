/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Consumer;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class City {

    ArrayList<Community> communityList;

    public ArrayList<Community> getCommunityList() {
        return communityList;
    }

    public void setCommunityList(ArrayList<Community> communityList) {
        this.communityList = communityList;
    }

    public City() {
        communityList = new ArrayList<>();

    }

    public Community addConsumers() {
        Community consumers = new Community();
        communityList.add(consumers);
        return consumers;
    }
}

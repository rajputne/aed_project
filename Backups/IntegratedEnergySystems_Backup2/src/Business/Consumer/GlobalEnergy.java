/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Consumer;

import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author Neerajsing Rajput
 */
public class GlobalEnergy {

    EnergyPerMonth energyPerMonth;
    String meterNumber;

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public EnergyPerMonth getEnergyPerMonth() {
        return energyPerMonth;
    }

    public void setEnergyPerMonth(EnergyPerMonth energyPerMonth) {
        this.energyPerMonth = energyPerMonth;
    }

    public GlobalEnergy() {
        energyPerMonth = new EnergyPerMonth();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Consumer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author neera
 */
public class EnergyPerDay {

    int day;
    int hour = 24;
    int minute = 60;
    int consumptionEveryMinutePerHour[][] = new int[24][60];
    ArrayList<Integer> meterReading;
    Random randomGenerator = new Random();

    public EnergyPerDay() {
        Calendar now = Calendar.getInstance();
        day = now.get(Calendar.DAY_OF_MONTH);
        meterReading = new ArrayList();
    }

    //Take the Consumption Remove this and take from Main 
    public int FillRandomValues() {
        for (int i = 0; i < consumptionEveryMinutePerHour.length; i++) {
            for (int j = 0; j < consumptionEveryMinutePerHour[0].length; j++) {
                consumptionEveryMinutePerHour[i][j] = randomGenerator.nextInt(300);
                System.out.println(consumptionEveryMinutePerHour[i][j]);
                return consumptionEveryMinutePerHour[i][j];
            }
        }

        return 0;
    }

    //This is the meter reading which will change every minute
    //Flaw the previous values will be removed as i am setting that to zero
    public void setConsumptionValues(int consumptionOfConsumer) {
        Calendar now = Calendar.getInstance();
        for (int i = 0; i < consumptionEveryMinutePerHour.length; i++) {
            // System.out.print("Row" + i);
            for (int j = 0; j < consumptionEveryMinutePerHour[0].length; j++) {
                if (i == now.get(Calendar.HOUR_OF_DAY) && j == now.get(Calendar.MINUTE)) {
                    //Get the Value of Device Consumption Here
                    consumptionEveryMinutePerHour[i][j] = consumptionOfConsumer;
                    meterReading.add(consumptionOfConsumer);
                    //Remove this After Usage
                    System.out.print(consumptionEveryMinutePerHour[i][j]);
                } //else {
                //Dont set this to zero every time store the previous Values;
                //consumptionEveryMinutePerHour[i][j] = 0;
                //Remove this After Usage
                //System.out.print(consumptionEveryMinutePerHour[i][j]);
                //}

            }
            System.out.println();
        }

    }

    public int sum_Of_Consumption_Per_Day() {
        int sum = 0;
        
        for (Integer i : meterReading) {
            sum += i.intValue();
        }
        return sum;
    }

}

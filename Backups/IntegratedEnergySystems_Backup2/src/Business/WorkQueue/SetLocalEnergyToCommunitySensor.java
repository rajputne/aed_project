/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Consumer.LocalEnergy;

/**
 *
 * @author neera
 */
public class SetLocalEnergyToCommunitySensor extends WorkRequest {
    
    LocalEnergy localEnergy;

    public LocalEnergy getLocalEnergy() {
        return localEnergy;
    }

    public void setLocalEnergy(LocalEnergy localEnergy) {
        this.localEnergy = localEnergy;
    }
    
    public SetLocalEnergyToCommunitySensor()
    {
        localEnergy=new LocalEnergy();
    }
    
}

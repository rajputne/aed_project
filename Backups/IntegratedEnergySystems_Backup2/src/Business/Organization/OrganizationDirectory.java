/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.GlobalEnergy.GlobaMaintainenceOrganization;
import Business.Organization.LocalEnergy.LocalAreaOrganization;
import Business.Organization.GlobalEnergy.GlobalManagerOrganization;
import Business.Organization.LocalEnergy.LocalManagerEnergyOrganization;
import Business.Organization.LocalEnergy.LocalAreaEnergyMaintainenceOrganization;
import Business.Organization.Organization.LocalOrganizationType;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createLocalOrganization(LocalOrganizationType type) {
        Organization organization = null;
        if (type.getValue().equals(LocalOrganizationType.Local.getValue())) {
            organization = new LocalAreaOrganization(LocalOrganizationType.Local.getValue());
            organizationList.add(organization);
        }

        return organization;
    }

    public Organization createGlobalEnergyOrganization(Organization.GlobalType type) {
        Organization organization = null;
        //Not using this for time being concentrating on other stuff

        boolean isOrganizationOfConsumerOrganizationExist = false;
        for (Organization o : organizationList) {
            if (o instanceof ConsumerOrganization) {
                isOrganizationOfConsumerOrganizationExist = true;
            } else {
                isOrganizationOfConsumerOrganizationExist = false;
            }
        }
        if (type.getValue().equals(Organization.GlobalType.GlobalMaintainence.getValue())) {
            organization = new GlobaMaintainenceOrganization();
            organizationList.add((GlobaMaintainenceOrganization) organization);
        } else if (type.getValue().equals(Organization.GlobalType.GlobalManager.getValue())) {
            organization = new GlobalManagerOrganization();
            organizationList.add((GlobalManagerOrganization) organization);
        } else if (type.getValue().equals(Organization.GlobalType.Consumer.getValue())) {
            if (!isOrganizationOfConsumerOrganizationExist) {
                organization = new ConsumerOrganization(Organization.GlobalType.Consumer.getValue());
                organizationList.add((ConsumerOrganization) organization);
            } else {
                JOptionPane.showMessageDialog(null, "You can add only one Consumer Organization");
            }
        }

        return organization;
    }

    public Organization createLocalEnergyOrganization(Organization.LocalType type) {
        Organization organization = null;

        if (type.getValue().equals(Organization.LocalType.LocalAreaAdmin.getValue())) {
            organization = new LocalManagerEnergyOrganization();
            organizationList.add((LocalManagerEnergyOrganization) organization);
        } else if (type.getValue().equals(Organization.LocalType.LocalMaintainence.getValue())) {
            organization = new LocalAreaEnergyMaintainenceOrganization();
            organizationList.add((LocalAreaEnergyMaintainenceOrganization) organization);
        }

        return organization;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class EmployeeDirectory {

    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
        employeeList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

//Add enum Type of Employee
    public enum EmployeeType {

        Admin("Admin"), Employee("Employee"), Consumer("Consumer");
        private String value;

        private EmployeeType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Employee createEmployee(String name) {
        Employee employee = new Employee();
        employee.setName(name);
        employeeList.add(employee);
        return employee;
    }
    
    public Consumer createConsumer(String name) {
        Consumer consumer = new Consumer();
        consumer.setName(name);
        employeeList.add(consumer);
        return consumer;
    }
}

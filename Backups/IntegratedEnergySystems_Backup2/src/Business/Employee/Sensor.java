/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import Business.Consumer.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Neerajsing Rajput
 */
public class Sensor {

    private ConsumerBattery battery;
    private Integer consumptionCalculation;
    private String uniqueSensorId;
    private int meter;
    int consumptionEveryMinutePerHour[][] = new int[24][60];
    ArrayList<Integer> meterReading;

    public String getUniqueSensorId() {
        return uniqueSensorId;
    }

    public void setUniqueSensorId(String uniqueSensorId) {
        this.uniqueSensorId = uniqueSensorId;
    }

    public ConsumerBattery getBattery() {
        return battery;
    }

    public void setBattery(ConsumerBattery battery) {
        this.battery = battery;
    }

    //Since Prof Bugrara mentioned that datapoints have to be captured My Datapoints
    private Map<Date, Integer> globalConsumptionList;
    private Map<Date, Integer> localConsumptionList;
    public Map<Date, Integer> tempValues;

    public Map<Date, Integer> getGlobalConsumptionList() {
        return globalConsumptionList;
    }

    public void setGlobalConsumptionList(Map<Date, Integer> globalConsumptionList) {
        this.globalConsumptionList = globalConsumptionList;
    }

    public Map<Date, Integer> getLocalConsumptionList() {
        return localConsumptionList;
    }

    public void setLocalConsumptionList(Map<Date, Integer> localConsumptionList) {
        this.localConsumptionList = localConsumptionList;
    }

    public Sensor() {
        battery = new ConsumerBattery();

        globalConsumptionList = new HashMap<>();
        localConsumptionList = new HashMap<>();
        tempValues = new HashMap<Date, Integer>();
        consumptionCalculation = 0;
        meterReading = new ArrayList<>();
    }

    public String smartSwitch(CommunitySensor cs) {
        //First check the charge in the LocalArea and then perform the operation
        String Message = "";
        LocalEnergy le = cs.rechargeLocalBattery();
        ConsumerBattery b = getBattery();
        //To See weather the conumption is done or not
        int consumptionByAllDevices = b.getTotalConsumptionByAllDevices();
        int voltageInConsumerBatteryRemaining = 0;

        for (Battery bat : le.getLocalAreabattery().getLocalbatteryList()) {
            if (bat.getBatteryVolts() > CommunitySensor.MIN_VOLTAGE) {
                voltageInConsumerBatteryRemaining = b.calculateVoltageInConsumerBattery();
                break;
            }
        }
        //When it goes to global the battery volt is reduced which should not happen 
        //Check whether the Consumer Battery has power to support its device 

        Collections.sort(le.getLocalAreabattery().getLocalbatteryList());

        if (voltageInConsumerBatteryRemaining <= CommunitySensor.MIN_VOLTAGE && b.getBatteryVolts() > 0) {

            //Take From the Local Energy Bank First
            for (Battery localAreaBattery : le.getLocalAreabattery().getLocalbatteryList()) {
                //Check the all local Area battery should be above 306 voltage
                //Find the Maximum value in this localAreaBattery arrayList and store that 
                //in a variable then apply the belwo check

                Battery maxBattery = le.getLocalAreabattery().getLocalbatteryList().get(le.getLocalAreabattery().getLocalbatteryList().size() - 1);
                if (maxBattery.getBatteryVolts() > CommunitySensor.MIN_VOLTAGE) {

                    if (localAreaBattery.getBatteryVolts() > 0) {
                        //Check whether the local Battery is the maxBattery Battery
                        if (maxBattery.getBatteryVolts() == localAreaBattery.getBatteryVolts()) {
                            //Collections.sort(le.getLocalAreabattery().getLocalbatteryList(), orderById);
                            //Recharge consumer battery if it gets too low we dont want to go below 306
                            if (b.getBatteryVolts() < CommunitySensor.MIN_VOLTAGE) {
                                int threshold = CommunitySensor.MAX_VOLTAGE - b.getBatteryVolts();
                                b.setBatteryVolts(threshold + b.getBatteryVolts());
                                //Discharge Local Battery
                                localAreaBattery.setBatteryVolts(maxBattery.getBatteryVolts() - threshold);

                            } else {
                                b.setBatteryVolts(consumptionByAllDevices + b.getBatteryVolts());
                                //Discharge Local Battery
                                localAreaBattery.setBatteryVolts(maxBattery.getBatteryVolts() - b.getTotalConsumptionByAllDevices());
                            }
                            Date date = new Date();
                            tempValues.put(date, b.getTotalConsumptionByAllDevices());
                            setLocalConsumptionList(tempValues);

                            Message = "The Local Energy is switch On and the consmption made by you" + b.getTotalConsumptionByAllDevices() + "Battery Volts" + b.getBatteryVolts();
                            return Message;
                        }
                    }
                } else //Plan is to check the consumer battery once again to see if the volatage is still der
                 if (b.getBatteryVolts() > CommunitySensor.MIN_VOLTAGE) {
                        b.setBatteryVolts(b.getBatteryVolts() - b.getTotalConsumptionByAllDevices());
                        return "Taken from Consumer Battery" + b.getBatteryVolts();
                    } else {
                        meter = b.getTotalConsumptionByAllDevices();
                        Date date = new Date();
                        tempValues.put(date, meter);
                        setGlobalConsumptionList(tempValues);
                        meterReading.add(meter);
                        //Call the localArea 
                        le = cs.rechargeLocalBattery();
                        //Set this to Minimum charged range
                        return "The Global Energy is switch On and the consmption made by you" + b.getTotalConsumptionByAllDevices() + "Battery Voltes" + b.getBatteryVolts();
                    }
            }
        } else {
            //Get voltage from the consumer battery
            int abc = b.getBatteryVolts() - b.getTotalConsumptionByAllDevices();
            if (abc < CommunitySensor.MIN_VOLTAGE) {

                return "Since the charge cannot go below 306 check hence global consumption happening";
            } else {
                b.setBatteryVolts(abc);
                return "No need to charge the battery since you already had requierd power from your battery" + b.getBatteryVolts();

            }

        }
        return "No No";
    }

    public int getElectricityAtThisMoment() {
        int sum = 0;
        for (Integer i : getGlobalConsumptionList().values()) {
            sum += i;
        }
        return sum;
    }

    public int sum_Of_Consumption_Per_Day() {
        int sum = 0;

        for (Integer i : meterReading) {
            sum += i.intValue();
        }
        return sum;
    }

}

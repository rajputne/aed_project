/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class Device {
    private String deviceName;
    private int deviceConsumption;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getDeviceConsumption() {
        return deviceConsumption;
    }

    public void setDeviceConsumption(int deviceConsumption) {
        this.deviceConsumption = deviceConsumption;
    }
    
}

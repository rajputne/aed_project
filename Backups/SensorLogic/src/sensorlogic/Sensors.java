/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Neerajsing Rajput
 */
public class Sensors {

    private LocalEnergy localEnergy;
    private GlobalEnergy globalEnergy;
    private int thresholdToRunHouse;

    public Sensors() {
        localEnergy = new LocalEnergy();
        globalEnergy = new GlobalEnergy();
        thresholdToRunHouse = 240;
    }

    public LocalEnergy getLocalEnergy() {
        return localEnergy;
    }

    public void setLocalEnergy(LocalEnergy localEnergy) {
        this.localEnergy = localEnergy;
    }

    public GlobalEnergy getGlobalEnergy() {
        return globalEnergy;
    }

    public void setGlobalEnergy(GlobalEnergy globalEnergy) {
        this.globalEnergy = globalEnergy;
    }

    public String switchEnergy(int temp, int windVelocity, Consumers consumers) {
        if (getLocalEnergy().getTempereture() < temp || getLocalEnergy().getWindVelocity() < windVelocity) {
            for (Consumer c : consumers.getConsumers()) {
                if (c.getBatteryPower() == 0) {
                    int newConsumption = getGlobalEnergy().getEnergyInKw() - thresholdToRunHouse;
                    GlobalEnergy ge = new GlobalEnergy();
                    ge.setEnergyInKw(newConsumption);
                    setGlobalEnergy(ge);
                    GlobalEnergyAdmin.setGlobalEnergy(globalEnergy);
                    if (c.getGlobalConsumption() == 0) {
                        c.setGlobalConsumption(thresholdToRunHouse);
                        Map<Date, Integer> tempValues = new HashMap<Date, Integer>();
                        Date date = new Date();
                        tempValues.put(date, thresholdToRunHouse);
                        c.setGlobalConsumptionList(tempValues);

                    } else {
                        int newValue = c.getGlobalConsumption() + thresholdToRunHouse;
                        c.setGlobalConsumption(newValue);
                        Map<Date, Integer> tempValues = new HashMap<Date, Integer>();
                        Date date = new Date();
                        tempValues.put(date, newValue);
                        c.setGlobalConsumptionList(tempValues);

                    }
                }
                return "The Global energy is switched On";
            }
        } else {
            for (Consumer c : consumers.getConsumers()) {
                //This is for Admins
                int localConsumption = getLocalEnergy().getEnergyInKw() - thresholdToRunHouse;
                LocalEnergy le = new LocalEnergy();
                le.setEnergyInKw(localConsumption);
                setLocalEnergy(le);

                if (c.getLocalConsumption() == 0) {
                    c.setLocalConsumption(thresholdToRunHouse);

                    Map<Date, Integer> tempValues = new HashMap<Date, Integer>();
                    Date date = new Date();
                    tempValues.put(date, thresholdToRunHouse);
                    c.setLocalConsumptionList(tempValues);

                } else {
                    int newValue = c.getLocalConsumption() + thresholdToRunHouse;
                    c.setLocalConsumption(newValue);
                    Map<Date, Integer> tempValues = new HashMap<Date, Integer>();
                    Date date = new Date();
                    tempValues.put(date, newValue);
                    c.setLocalConsumptionList(tempValues);
                }
            }
        }
        return "Enjoy the free energy";

    }

}

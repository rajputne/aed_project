/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class Consumers {

    ArrayList<Consumer> consumers = new ArrayList<>();
    private Sensors sensor;

    public ArrayList<Consumer> getConsumers() {
        return consumers;
    }

    public void setConsumers(ArrayList<Consumer> consumers) {
        this.consumers = consumers;
    }

    public Consumers() {
        sensor = new Sensors();
    }

    public Sensors getSensor() {
        return sensor;
    }

    public void setSensor(Sensors sensor) {
        this.sensor = sensor;
    }

    public Consumer addConsumer() {
        Consumer consumer = new Consumer();
        consumers.add(consumer);
        return consumer;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization.GlobalEnergy;

import Business.Organization.Organization;
import Business.Role.GlobalEnterprise.GlobalOrganizations.GlobalEnergyOrganizationRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class GlobalOrganization extends Organization {

    public static GlobalOrganization instance = null;
   
   public static GlobalOrganization getInstance(String name) {
      if(instance == null) {
         instance = new GlobalOrganization(name);
      }
      return instance;
   }
    public GlobalOrganization(String name) {
        super(name);

    }

  

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new GlobalEnergyOrganizationRole());
        return roles;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.GlobalEnergy.GlobaMaintainenceOrganization;
import Business.Organization.LocalEnergy.LocalAreaOrganization;
import Business.Organization.GlobalEnergy.GlobalManagerOrganization;
import Business.Organization.GlobalEnergy.GlobalOrganization;
import Business.Organization.LocalEnergy.LocalManagerEnergyOrganization;
import Business.Organization.LocalEnergy.LocalAreaEnergyMaintainenceOrganization;
import Business.Organization.Organization.GlobalOrganizationType;
import Business.Organization.Organization.LocalOrganizationType;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createGlobalOrganization(GlobalOrganizationType type) {
        Organization organization = null;
        //To check whether Global Organization Exists
        boolean isOrganizationOfGlobalOrgExist = false;
        for (Organization o : organizationList) {
            if (o instanceof GlobalOrganization) {
                isOrganizationOfGlobalOrgExist = true;
            } else {
                isOrganizationOfGlobalOrgExist = false;
            }
        }

        if (type.getValue().equals(GlobalOrganizationType.Global.getValue())) {

            if (!isOrganizationOfGlobalOrgExist) {
                organization = GlobalOrganization.getInstance(GlobalOrganizationType.Global.getValue());
                organizationList.add(organization);
            } else {
                JOptionPane.showMessageDialog(null, "You can Only add 1 Global Organization");
            }
        }

        return organization;
    }

    public Organization createLocalOrganization(LocalOrganizationType type) {
        Organization organization = null;
        if (type.getValue().equals(LocalOrganizationType.Local.getValue())) {
            organization = new LocalAreaOrganization(LocalOrganizationType.Local.getValue());
            organizationList.add(organization);
        }

        return organization;
    }

    public Organization createGlobalEnergyOrganization(Organization.GlobalType type) {
        Organization organization = null;

        if (type.getValue().equals(Organization.GlobalType.GlobalMaintainence.getValue())) {
            organization = new GlobaMaintainenceOrganization();
            organizationList.add((GlobaMaintainenceOrganization) organization);
        } else if (type.getValue().equals(Organization.GlobalType.GlobalManager.getValue())) {
            organization = new GlobalManagerOrganization();
            organizationList.add((GlobalManagerOrganization) organization);
        }else if (type.getValue().equals(Organization.GlobalType.Consumer.getValue())) {
            organization = new ConsumerOrganization(Organization.GlobalType.Consumer.getValue());
            organizationList.add((ConsumerOrganization) organization);
        }

        return organization;
    }

    public Organization createLocalEnergyOrganization(Organization.LocalType type) {
        Organization organization = null;

        if (type.getValue().equals(Organization.LocalType.LocalAreaAdmin.getValue())) {
            organization = new LocalManagerEnergyOrganization();
            organizationList.add((LocalManagerEnergyOrganization) organization);
        } else if (type.getValue().equals(Organization.LocalType.LocalMaintainence.getValue())) {
            organization = new LocalAreaEnergyMaintainenceOrganization();
            organizationList.add((LocalAreaEnergyMaintainenceOrganization) organization);
        }
        

        return organization;
    }
    
    

}

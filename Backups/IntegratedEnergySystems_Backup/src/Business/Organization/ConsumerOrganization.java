/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Consumer.Consumer;
import Business.Consumer.Sensor;
import Business.Organization.GlobalEnergy.GlobalOrganization;

import Business.Role.ConsumerRole.ConsumerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author neera
 */
public class ConsumerOrganization extends Organization {
    
   private String consumerName;
    private String consumerId;
    private String address;

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }
    private int LocalAreaCode;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getLocalAreaCode() {
        return LocalAreaCode;
    }

    public void setLocalAreaCode(int LocalAreaCode) {
        this.LocalAreaCode = LocalAreaCode;
    }

    public String getConsumerId() {
        
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }
    
    private Sensor sensor;

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }


    

   
    
    Consumer consumer;

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public ConsumerOrganization(String name) {
        super(name);
        consumer=new Consumer();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new ConsumerRole());
        return roles;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Consumer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author Neerajsing Rajput
 */
public class GlobalEnergyAdmin {

    private static GlobalEnergy globalEnergy;
    public City city;
    private double chargePerWattHour = 0.011;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public static GlobalEnergy getGlobalEnergy() {
        return globalEnergy;
    }

    public static void setGlobalEnergy(GlobalEnergy globalEnergy) {
        GlobalEnergyAdmin.globalEnergy = globalEnergy;
    }

    public double generateConsumerMonthlyBill(Consumer consumer) {
        int sum = 0;

        for (Community cs : getCity().getCommunityList()) {
            for (Consumer c : cs.getConsumerList()) {
                if (c.equals(consumer)) {
                    sum += calculateEnergyOfCurrentMonth(c.getSensor());

                }
            }
        }
        double chargePerMonth = sum * chargePerWattHour;
        return chargePerMonth;
    }

    public int calculateCustomizedEnergyConsumption(Date fromDate, Date toDate, Consumer c) {

        int dayDiff = calculateDayDifference(fromDate, toDate);
        int sum = 0;
        for (int i = 0; i < dayDiff - 1; i++) {
            sum = c.getSensor().sum_Of_Consumption_Per_Day();
        }
        return sum;
    }

    public int calculateDayDifference(Date fromDate, Date toDate) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String dateFrom = format.format(fromDate);
        String dateTo = format.format(toDate);
        Date d1 = null;
        Date d22 = null;
        int daysInBetween = 0;
        try {
            d1 = format.parse(dateFrom);
            d22 = format.parse(dateTo);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d22);
            daysInBetween = Days.daysBetween(dt1, dt2).getDays();
            //System.out.print(Days.daysBetween(dt1, dt2).getDays() + " days, ");
            //System.out.print(Hours.hoursBetween(dt1, dt2).getHours() % 24 + " hours, ");
            //System.out.print(Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 + " minutes, ");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return daysInBetween;

    }

    public int calculateEnergyOfCurrentMonth(Sensor s) {
        //Remove this After Usage
        int month = month = new GregorianCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
        int sum = 0;
        switch (month) {
            case 31:
                for (int i = 0; i < 31; i++) {
                    sum = s.sum_Of_Consumption_Per_Day();
                }
                break;
            case 30:
                for (int i = 0; i < 31; i++) {
                    sum = s.sum_Of_Consumption_Per_Day();
                }
                break;
            case 29:
                for (int i = 0; i < 29; i++) {
                    sum = s.sum_Of_Consumption_Per_Day();
                }
                break;
            case 28:
                for (int i = 0; i < 29; i++) {
                    sum = s.sum_Of_Consumption_Per_Day();
                }
                break;
            default:
                break;
        }
        return sum;
    }

}

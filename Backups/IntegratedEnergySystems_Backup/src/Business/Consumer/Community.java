/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Consumer;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class Community {

    private String communityName;

    private int communityId;
    
    private CommunitySensor communitySensor;

    public CommunitySensor getCommunitySensor() {
        return communitySensor;
    }

    public void setCommunitySensor(CommunitySensor communitySensor) {
        this.communitySensor = communitySensor;
    }

   
   
   
   
    
    
    ArrayList<Consumer> consumerList;

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }

    public ArrayList<Consumer> getConsumerList() {
        return consumerList;
    }

    public void setConsumerList(ArrayList<Consumer> consumerList) {
        this.consumerList = consumerList;
    }

    public Community() {
        consumerList = new ArrayList<>();
    }

    public Consumer addConsumer() {
        Consumer consumer = new Consumer();
        consumerList.add(consumer);
        return consumer;
    }
    
    
}

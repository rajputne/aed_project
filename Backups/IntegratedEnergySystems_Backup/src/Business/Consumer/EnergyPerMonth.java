/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Consumer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.GregorianCalendar;
import java.util.Random;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author neera
 */
public class EnergyPerMonth {

    EnergyPerDay epd;
    double bill_amt_per_watt_hour = 0.011;

    public EnergyPerDay getEpd() {
        return epd;
    }

    public void setEpd(EnergyPerDay epd) {
        this.epd = epd;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public ArrayList<Integer> getMeterReadingPerMonth() {
        return meterReadingPerMonth;
    }

    public void setMeterReadingPerMonth(ArrayList<Integer> meterReadingPerMonth) {
        this.meterReadingPerMonth = meterReadingPerMonth;
    }
    int month;
    ArrayList<Integer> meterReadingPerMonth;

    public void setMeterToZeroAtEveryEndOfTheMonth() {

    }

    EnergyPerMonth() {
        month = new GregorianCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
        epd = new EnergyPerDay();
    }

    public void fillRandomMonthValues() {
        Random randomGenerator = new Random();
        for (int k = 0; k < 30; k++) {

            for (int i = 0; i < epd.consumptionEveryMinutePerHour.length; i++) {
                //System.out.println("Row" + i);
                for (int j = 0; j < epd.consumptionEveryMinutePerHour[0].length; j++) {
                    epd.consumptionEveryMinutePerHour[i][j] = randomGenerator.nextInt(9);
                    //System.out.print(" " + epd.consumptionEveryMinutePerHour[i][j]);

                }
            }
        }
    }

    public int calculateCustomizedEnergyConsumption(Date fromDate, Date toDate) {
        int dayDiff = calculateDayDifference(fromDate, toDate);
        int sum = 0;
        for (int i = 0; i < dayDiff - 1; i++) {
            sum = getEpd().sum_Of_Consumption_Per_Day();
        }
        return sum;
    }

    public int calculateDayDifference(Date fromDate, Date toDate) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String dateFrom = format.format(fromDate);
        String dateTo = format.format(toDate);
        Date d1 = null;
        Date d22 = null;
        int daysInBetween = 0;
        try {
            d1 = format.parse(dateFrom);
            d22 = format.parse(dateTo);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d22);
            daysInBetween = Days.daysBetween(dt1, dt2).getDays();
            //System.out.print(Days.daysBetween(dt1, dt2).getDays() + " days, ");
            //System.out.print(Hours.hoursBetween(dt1, dt2).getHours() % 24 + " hours, ");
            //System.out.print(Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 + " minutes, ");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return daysInBetween;

    }

    public int calculateEnergyOfCurrentMonth() {
        //Remove this After Usage
        fillRandomMonthValues();
        int sum = 0;
        switch (month) {
            case 31:
                for (int i = 0; i < 31; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            case 30:
                for (int i = 0; i < 31; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            case 29:
                for (int i = 0; i < 29; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            case 28:
                for (int i = 0; i < 29; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            default:
                break;
        }
        return sum;
    }

    public int calculateEnergyOfAnyMonth(int month) {
        fillRandomMonthValues();
        int sum = 0;
        switch (month) {
            case 31:
                for (int i = 0; i < 31; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            case 30:
                for (int i = 0; i < 31; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            case 29:
                for (int i = 0; i < 29; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            case 28:
                for (int i = 0; i < 29; i++) {
                    sum = getEpd().sum_Of_Consumption_Per_Day();
                }
                break;
            default:
                break;
        }
        return sum;

    }

    public double calc_Current_Monthly_Bill() {
        //Calendar now = Calendar.getInstance();
        //int month = now.get(Calendar.MONTH);
        int energyPerMonthConsumed = calculateEnergyOfCurrentMonth();
        double bill = energyPerMonthConsumed * bill_amt_per_watt_hour;
        return bill;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Consumer.CommunitySensor;

/**
 *
 * @author neera
 */
public class SetConsumerToLocalEnergy extends WorkRequest {

    CommunitySensor communitySensor;

    public CommunitySensor getCommunitySensor() {
        return communitySensor;
    }

    public void setCommunitySensor(CommunitySensor communitySensor) {
        this.communitySensor = communitySensor;
    }

    public SetConsumerToLocalEnergy() {
        communitySensor = new CommunitySensor();
    }

}

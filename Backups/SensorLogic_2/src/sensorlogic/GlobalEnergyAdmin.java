/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class GlobalEnergyAdmin {
    private static GlobalEnergy globalEnergy;
    private Consumers consumers;

    public static GlobalEnergy getGlobalEnergy() {
        return globalEnergy;
    }

    public static void setGlobalEnergy(GlobalEnergy globalEnergy) {
        GlobalEnergyAdmin.globalEnergy = globalEnergy;
    }

    

    public Consumers getConsumers() {
        return consumers;
    }

    public void setConsumers(Consumers consumers) {
        this.consumers = consumers;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Neerajsing Rajput
 */
public class Sensor {

    private GlobalEnergy globalEnergy;
    private LocalEnergy localEnergy;
    private ConsumerBattery battery;
    private Integer consumptionCalculation;

    public ConsumerBattery getBattery() {
        return battery;
    }

    public void setBattery(ConsumerBattery battery) {
        this.battery = battery;
    }

    //Since Prof Bugrara mentioned that datapoints have to be captured My Datapoints
    private Map<Date, Integer> globalConsumptionList;
    private Map<Date, Integer> localConsumptionList;
    public Map<Date, Integer> tempValues;

    public Map<Date, Integer> getGlobalConsumptionList() {
        return globalConsumptionList;
    }

    public void setGlobalConsumptionList(Map<Date, Integer> globalConsumptionList) {
        this.globalConsumptionList = globalConsumptionList;
    }

    public Map<Date, Integer> getLocalConsumptionList() {
        return localConsumptionList;
    }

    public void setLocalConsumptionList(Map<Date, Integer> localConsumptionList) {
        this.localConsumptionList = localConsumptionList;
    }

    public Sensor() {
        battery = new ConsumerBattery();
        localEnergy = new LocalEnergy();
        globalEnergy = new GlobalEnergy();
        globalConsumptionList = new HashMap<>();
        localConsumptionList = new HashMap<>();
        tempValues = new HashMap<Date, Integer>();
        consumptionCalculation = 0;
    }

    public GlobalEnergy getGlobalEnergy() {
        return globalEnergy;
    }

    public void setGlobalEnergy(GlobalEnergy globalEnergy) {
        this.globalEnergy = globalEnergy;
    }

    public LocalEnergy getLocalEnergy() {
        return localEnergy;
    }

    public void setLocalEnergy(LocalEnergy localEnergy) {
        this.localEnergy = localEnergy;
    }

    public String smartSwitch() {
        //First check the charge in the LocalArea and then perform the operation
        setLocalEnergy(AreaSensor.rechargeLocalBattery());

        GlobalEnergy ge = getGlobalEnergy();
        ConsumerBattery bat = getBattery();

        int consumptionByAllDevices = bat.getTotalConsumptionByAllDevices();
        int consumerBatteryDrained = bat.calculatePowerInConsumerBattery();
        LocalAreaBatteries[] batteryCells = getLocalEnergy().getLocalAreabatteryCells();

        if (consumerBatteryDrained < 306) {
            for (Battery battery : bat.getConsumerBatteryArray()) {      
                    if (battery.getBatteryVolts() < 14 && battery.getBatteryVolts() >= 0) {
                        //Getting from the LocalBank
                        for (int i = 0; i < batteryCells.length; i++) {
                            
                        }
                        return "Local energy Consumed" + battery.getBatteryVolts();
                    }
                
            }

        }

        return "";
    }
}

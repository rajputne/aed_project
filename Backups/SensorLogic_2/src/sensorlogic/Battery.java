/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class Battery {

    private int batteryVolts;

    public Battery() {
        batteryVolts=14;
    }

    public int getBatteryVolts() {
        return batteryVolts;
    }

    public void setBatteryVolts(int batteryVolts) {
        this.batteryVolts = batteryVolts;
    }

    

}

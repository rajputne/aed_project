/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

import java.util.ArrayList;

/**
 *
 * @author Neerajsing Rajput
 */
public class LocalAreaBatteries {

    Battery[] localbatteryArray;

    public Battery[] getLocalbatteryArray() {
        return localbatteryArray;
    }

    public void setLocalbatteryArray(Battery[] localbatteryArray) {
        this.localbatteryArray = localbatteryArray;
    }

    public LocalAreaBatteries() {
        localbatteryArray = new Battery[25];
    }

}

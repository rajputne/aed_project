/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sensorlogic;

/**
 *
 * @author Neerajsing Rajput
 */
public class LocalEnergy extends Weather {

    private LocalAreaBatteries[] localAreabatteryCells;

    public LocalAreaBatteries[] getLocalAreabatteryCells() {
        return localAreabatteryCells;
    }

    public void setLocalAreabatteryCells(LocalAreaBatteries[] localAreabatteryCells) {
        this.localAreabatteryCells = localAreabatteryCells;
    }

    public LocalEnergy() {
        localAreabatteryCells = new LocalAreaBatteries[15];
    }

    public int getLocalEnergyPower() {
        int energyInTheLocalBank = 0;
        for (int i = 0; i < getLocalAreabatteryCells().length; i++) {
            for (int j = 0; j < getLocalAreabatteryCells()[i].getLocalbatteryArray().length; j++) {
                //System.out.println(localbatteryArray[j].getBatteryVolts());
                energyInTheLocalBank += getLocalAreabatteryCells()[i].getLocalbatteryArray()[j].getBatteryVolts();

            }

        }
        return energyInTheLocalBank;
    }

}
